@echo off
echo Setting things up...
echo
docker-compose up -d test-db
timeout /T 30 /NOBREAK
psql -U pgsql_user -d pgsql_data -h localhost -p 5432 -f ./DB/db_dump.sql
echo Starting unit tests...
echo
python -m unittest discover -s tests/unit -t tests/unit
echo Unit tests done!
echo
echo Starting Integration tests...
echo
python -m unittest discover -s tests/integration -t tests/integration
echo Integration tests done!
echo
echo Cleaning up...
docker-compose down test-db