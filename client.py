#!/usr/bin/env python3
import socket
import json
from src.ClientMethods import ClientMethods

_USER: dict = {}

PORT: int = 8081  # server port
ADDRESS: str = "127.0.0.1"  # server address

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((ADDRESS, PORT))

    # client main loop
    while True:
        user_input: str = str(input()).strip().lower()

        if user_input == '':
            pass
        else:

            if user_input == 'quit':
                data_out = ClientMethods.prepare_data('quit')
                s.send(json.dumps(data_out).encode())
                break

            if user_input == 'help':
                if len(_USER) > 0:
                    data_out = ClientMethods.prepare_data('help', uid=_USER['id'], auth=_USER['auth'])
                else:
                    data_out = ClientMethods.prepare_data('help')
                s.send(json.dumps(data_out).encode())
                data_in: str = s.recv(2048).decode()  # received data
                print(data_in)

            elif user_input == 'login':
                data_out = ClientMethods.prepare_data('login', username=input('Insert username: '),
                                                      password=input('Insert password: '))
                s.send(json.dumps(data_out).encode())
                data_in: dict = json.loads(s.recv(1024))  # received data
                if len(data_in) > 1:
                    _USER = data_in['user']
                    print(data_in['message'])
                else:
                    print(data_in)

            elif user_input == 'register':
                data_out = ClientMethods.prepare_data('register', username=input('Insert username: '),
                                                      password=input('Insert password: '),
                                                      repeat_password=input('Repeat password: '))
                s.send(json.dumps(data_out).encode())
                data_in: dict = json.loads(s.recv(1024).decode())  # received data
                if len(data_in) > 1:
                    _USER = data_in['user']
                    print(data_in['message'])
                else:
                    print(data_in)

            elif user_input == 'logout' and _USER:
                data_out = ClientMethods.prepare_data('logout', uid=_USER['id'], auth=_USER['auth'])
                s.send(json.dumps(data_out).encode())
                data_in: str = s.recv(1024).decode()  # received data
                print(data_in)
                _USER = {}

            # admin commands
            elif user_input == 'info' and _USER:
                data_out = ClientMethods.prepare_data('info', auth=_USER['auth'])
                s.send(json.dumps(data_out).encode())
                data_in: str = s.recv(1024).decode()  # received data
                print(data_in)

            elif user_input == 'uptime' and _USER:
                data_out = ClientMethods.prepare_data('uptime', auth=_USER['auth'])
                s.send(json.dumps(data_out).encode())
                data_in: str = s.recv(1024).decode()  # received data
                print(data_in)

            elif user_input == 'adduser' and _USER:
                data_out = ClientMethods.prepare_data('addUser', auth=_USER['auth'],
                                                      username=input('Insert username: '),
                                                      password=input('Insert password: '),
                                                      authorization=input('Insert authentication: '))
                s.send(json.dumps(data_out).encode())
                data_in: str = s.recv(1024).decode()  # received data
                print(data_in)

            elif user_input == 'changeuser' and _USER:
                data_out = ClientMethods.prepare_data('changeUser', auth=_USER['auth'],
                                                      user_id=input('Insert user id: '),
                                                      username=input('Insert username: '),
                                                      password=input('Insert password: '),
                                                      authorization=input('Insert authentication: '))
                s.send(json.dumps(data_out).encode())
                data_in: str = s.recv(1024).decode()  # received data
                print(data_in)

            elif user_input == 'deleteuser' and _USER:
                data_out = ClientMethods.prepare_data('deleteUser', auth=_USER['auth'],
                                                      user_id=input('Insert user id: '))
                s.send(json.dumps(data_out).encode())
                data_in: str = s.recv(1024).decode()  # received data
                print(data_in)

            elif user_input == 'deletemessage' and _USER:
                data_out = ClientMethods.prepare_data('deleteMessage', auth=_USER['auth'],
                                                      message_id=input('Insert message id: '))
                s.send(json.dumps(data_out).encode())
                data_in: str = s.recv(1024).decode()  # received data
                print(data_in)

            elif user_input == 'getusers' and _USER:
                data_out = ClientMethods.prepare_data('getUsers', auth=_USER['auth'])
                s.send(json.dumps(data_out).encode())
                data_in: str = s.recv(1024).decode()  # received data
                print(data_in)

            elif user_input == 'getmessages' and _USER:
                data_out = ClientMethods.prepare_data('getMessages', auth=_USER['auth'])
                s.send(json.dumps(data_out).encode())
                data_in: str = s.recv(1024).decode()  # received data
                print(data_in)

            elif user_input == 'getauths' and _USER:
                data_out = ClientMethods.prepare_data('getAuths', auth=_USER['auth'])
                s.send(json.dumps(data_out).encode())
                data_in: str = s.recv(1024).decode()  # received data
                print(data_in)

            elif user_input == 'communicate' and _USER:
                data_out = ClientMethods.prepare_data('communicate', uid=_USER['id'], auth=_USER['auth'],
                                                      text=input('Enter message text (max 255 characters): '))
                s.send(json.dumps(data_out).encode())
                data_in: str = s.recv(1024).decode()  # received data
                print(data_in)

            # logged user commands
            elif user_input == 'message' and _USER:
                data_out = ClientMethods.prepare_data('message', uid=_USER['id'], auth=_USER['auth'],
                                                      user=input('Enter username of receiver: '),
                                                      text=input('Enter message text (max 255 characters): '))
                s.send(json.dumps(data_out).encode())
                data_in: str = s.recv(1024).decode()  # received data
                print(data_in)

            elif user_input == 'readmessages' and _USER:
                data_out = ClientMethods.prepare_data('readMessages', uid=_USER['id'], auth=_USER['auth'],
                                                      user=input('Enter username of receiver: '))
                s.send(json.dumps(data_out).encode())
                data_in: str = s.recv(1024).decode()  # received data
                print(data_in)

            elif user_input == 'readhistory' and _USER:
                data_out = ClientMethods.prepare_data('readHistory', uid=_USER['id'], auth=_USER['auth'])
                s.send(json.dumps(data_out).encode())
                data_in: str = s.recv(1024).decode()  # received data
                print(data_in)

            elif user_input == 'cleaninbox' and _USER:
                data_out = ClientMethods.prepare_data('cleanInbox', uid=_USER['id'], auth=_USER['auth'])
                s.send(json.dumps(data_out).encode())
                data_in: str = s.recv(1024).decode()  # received data
                print(data_in)

            elif user_input == 'changepassword' and _USER:
                data_out = ClientMethods.prepare_data('changePassword', uid=_USER['id'],
                                                      auth=_USER['auth'],
                                                      password=input('Insert password: '),)
                s.send(json.dumps(data_out).encode())
                data_in: str = s.recv(1024).decode()  # received data
                print(data_in)

            else:
                data_out = ClientMethods.prepare_data(' ')
                s.send(json.dumps(data_out).encode())
                data_in: str = s.recv(1024).decode()  # received data
                print(data_in)
