import psycopg2
from DB.config import config


def connect() -> any:
    """
    Creates and return cursor for data manipulation
    :return: any | cursor
    """
    conn: any = None
    try:
        params: dict = config()
        conn = psycopg2.connect(**params)
        cursor = conn.cursor()
        return conn, cursor
    except (Exception, psycopg2.DatabaseError) as e:
        print('DB connection error: {}'.format(e))
    finally:
        if conn is None:
            return None


if __name__ == '__main__':
    try:
        connection, cur = connect()
        cur.execute('SELECT version()')
        printer = cur.fetchall()
        print(printer)
    except Exception as err:
        print(err)
    finally:
        cur.close()
        connection.close()
