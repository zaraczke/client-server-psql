DROP TABLE IF EXISTS message_history;
DROP TABLE IF EXISTS message;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS auth;


CREATE TABLE auth (
    id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    auth VARCHAR(12) NOT NULL UNIQUE
);

CREATE TABLE users (
    id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    username VARCHAR(25) NOT NULL UNIQUE,
    password CHAR(32) NOT NULL,
    auth_id INT NOT NULL references auth(id)
);

CREATE TABLE message(
    id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    text VARCHAR(400) NOT NULL,
    sender_id INT NOT NULL references users(id)
);

CREATE TABLE message_history(
    message_id INT NOT NULL references message(id),
    receiver_id INT NOT NULL references users(id),
    PRIMARY KEY(message_id, receiver_id)
);

INSERT INTO auth (auth) VALUES ('admin');
INSERT INTO auth (auth) VALUES ('user');
INSERT INTO users (username, password, auth_id) VALUES ('admin', '21232f297a57a5a743894a0e4a801fc3', 1);
INSERT INTO users (username, password, auth_id) VALUES ('user', 'ee11cbb19052e40b07aac0ca060c23ee', 2);